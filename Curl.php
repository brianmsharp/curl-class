<?php

/**
 * This class uses curl to retrieve header, body and other info from a given url
 */
class SeoCurl
{
    /** @var resource cURL handle */
    protected $ch;

    /** @var string starting url */
    public $url;

    /** @var array Curl info as array, no body or header */
    public $info = [];

    /** @var string Header */
    protected $header;

    /** @var mixed Body */
    protected $body;

    /** @var mixed File size */
    protected $file_size;

    /** @var string User agent used with request */
    protected $user_agent;

    /** @var bool Option to follow redirects */
    public $follow_location = true;

    /** @var array Curl error */
	public $error = [];
	
    /** @var array Headers used with header callback functiom */
	public $headers = [];


    /**
     * @param string $url
     * @param string $user_agent
     */
    public function __construct($url, $user_agent = "chrome")
    {
        $this->user_agent = $user_agent;
        $this->url = $url;
        $this->ch  = curl_init($url);
    }


    /**
	 * Make a new url request using the same handle
     * @param string $url
     */
    public function recycle_handle($url)
    {
        $this->url = $url;
        $this->ch  = curl_init($url);
    }


    /**
     * Set curl handle options
     *
     * @param options curl options
     */
    protected function set_options( $options = array() )
    {
        foreach ($options as $key => $val) {
            curl_setopt($this->ch, $key, $val);
        }

        switch($this->user_agent)
        {
            case 'firefox':
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31'
                );
            break;
            case 'chrome':
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2'
                );
            break;
            case 'internet explorer 10';
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Mozilla/1.22 (compatible; MSIE 10.0; Windows 3.1)'
                );
            break;
            case 'internet explorer 6':
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Mozilla/4.08 (compatible; MSIE 6.0; Windows NT 5.1)'
                );
            break;
            case 'googlebot':
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Googlebot/2.1 (+http://www.google.com/bot.html)'
                );
            break;
            case 'bingbot':
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)'
                );
            break;
            case 'internet explorer 11':
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko'
                );
            break;
            case 'opera':
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.14'
                );
            break;
            case 'safari':
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A'
                );
            break;
            case 'twitter':
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Twitterbot/1.0'
                );
                break;
            case 'facebook':
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'facebookexternalhit/1.1 (+https://www.facebook.com/externalhit_uatext.php)'
                );
            break;
            default:
                curl_setopt($this->ch, CURLOPT_USERAGENT,
                    'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31'
                );
        }

        curl_setopt($this->ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, $this->follow_location);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
    }


    /**
     * Set user-agent
     * @param string
     */
    public function user_agent($user_agent)
    {
        $this->user_agent = $user_agent;
    }

    /**
     * Sets info property and returns true or false for success
     * @return bool
     */
    public function get_info()
    {
        if ($this->info) {
             return true;
        }

        $this->set_options(array(
			CURLOPT_HEADER 	=> 0,
			CURLOPT_NOBODY 	=> 1,
        ));

		/* Execute request */
		curl_exec($this->ch);

		/* Get info */
		$this->info = curl_getinfo($this->ch);

		/* Handle errors */
        if (0 !== curl_errno($this->ch)) {
			$this->error = [
				'no' => curl_errno($this->ch),
				'error' => curl_error($this->ch)
			];
			return false;
        }

        return true;
    }

    /**
     * Get header
     * @return string
     */
    public function get_header()
    {
        if ($this->header) {
            return $this->header;
        }

        $this->set_options(array(
			CURLOPT_HEADER 			=> 1,
			CURLOPT_NOBODY 			=> 1,
        ));

		/* Execute request */
        $response = curl_exec($this->ch);

		/* Handle errors */
        if (0 !== curl_errno($this->ch)) {
			$this->error = [
				'no' => curl_errno($this->ch),
				'error' => curl_error($this->ch)
			];
			return false;
        }

        return $this->header = $response;
    }


    /**
     * Get body
     * @return string
     */
    public function get_body()
    {
        if ($this->body) {
             return $this->body;
        }

        $this->set_options(array(
			CURLOPT_HEADER 			=> 0,
			CURLOPT_NOBODY 			=> 0,
        ));

        $response = curl_exec($this->ch);

		/* Handle errors */
        if (0 !== curl_errno($this->ch)) {
			$this->error = [
				'no' => curl_errno($this->ch),
				'error' => curl_error($this->ch)
			];
			return false;
        }

        return $this->body = $response;
    }

    /**
     * Get info, header and body
	 * NOTE:  The headers seem useless with curlinfo...  The callback function below may be usefull for tracking redirect chains
	 *   but I'm not using this function for now.
     * @return array
     */
    public function get_both()
    {
        if ($this->header && $this->body) {
			/* Return response */
			 return array(
				'header' => $this->header, 
				'body'	 => $this->body,
				'info'   => $this->info
			 );
        }


        $this->set_options(array(
				CURLOPT_HEADER => 1,
				CURLOPT_NOBODY => 0
		));

		$headers = [];
		curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, 
			function($curl, $header) use (&$headers){
				/** @var string This function fires for each line ($header) of the headers and must return $len */
				$len = strlen($header);

				/* Most header lines contain a key value pair delimited by ":" */
				if (strpos($header, ':') !== false) {
					$header = explode(':', $header, 2);

					/* If it's valid, save to $headers array */
					if (isset($header[1]))
						$headers[strtolower(trim($header[0]))][] = trim($header[1]);
				}
				else {					
					/* Check for http code */
					$header = explode(' ', $header);
					if (count($header) === 3 && preg_match('~\d\d\d~', $header[1]))
						$headers['http_code'][] = $header[1];
				}

				return $len;
		});

		/** Response */
		$response = curl_exec($this->ch);

		/* Set info */
		$this->info = curl_getinfo($this->ch);
		
		/* Handle errors */
        if (0 !== curl_errno($this->ch)) {
			$this->error = [
				'no' => curl_errno($this->ch),
				'error' => curl_error($this->ch)
			];
			return false;
        }

        return $response;
    }


    /**
     * Get file size
     * @return int
     */
    public function get_file_size()
    {
        if ($this->header && preg_match('/Content-Length: (\d+)/', $this->header, $matches)){
            return (int)$matches[1];
        }

        $this->set_options(array(
			CURLOPT_HEADER 			=> 1,
			CURLOPT_NOBODY 			=> 1,
        ));

        $response = curl_exec($this->ch);

		/* Handle errors */
        if (0 !== curl_errno($this->ch)) {
			$this->error = [
				'no' => curl_errno($this->ch),
				'error' => curl_error($this->ch)
			];
			return false;
        }

		return preg_match('/Content-Length: (\d+)/', $response, $matches) ? (int)$matches[1] : -1;
    }

	/**
	 * Response code after redirects.
	 * @return mixed int or false
	 */
	public function response_code(){

		return (int)$this->info['http_code'] ?: false ;
		
	}


	/**
	 * Get the number of redirects.
	 * @return int
	 */
	public function redirect_count(){

		return (int)$this->info['redirect_count'] ?: 0 ;
		
    }


    /**
     * Close handle
     * @return string
     */
    public function close(){

		if (is_resource($this->ch)) curl_close($this->ch);
		
    }



	/**
	 * Get the status code's description.
	 * @return int
	 */
	function get_status_desc(){

		$header = str_replace(["\r\n", "\r"], "\n",$this->header);
		$status_string = strtok($header, "\n");
		$status_desc = substr($status_string, (strpos($status_string, ' ', 1) + 1));

		return $status_desc;
	}



	/**
	 * Get the redirect URL when follow location is off.
	 * @return string url
	 */
	function redirect_url(){

		$header = str_replace(["\r\n", "\r"], "\n",$this->header);
		if (preg_match("~\n(?:Location|URI): *(.*?) *\n~", $header, $matches) ) {
            return $matches[1];
        }
        return null;
	}
}